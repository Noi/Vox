#version 450

struct voxel {
	vec4 center;
	vec4 color;
	vec4 material;
};

layout(set = 0, binding = 0) buffer Data {
	voxel data[];
};

layout(set = 0, binding = 1) buffer PosData {
	vec3 pos;
	float dir[2];
	int res[2];
};

layout(set = 0, binding = 2) buffer WorldData {
	float seed;
	uint count;
};

layout (location = 0) in vec2 inUV;

layout(location = 0) out vec4 fcolor;


float random(vec2 co) {
	return fract(sin(dot(co.xy, vec2(12.9898, 78.233))) * 43758.5453123);
}

/*vec3 Sun(vec3 rd) {
	vec3 light = normalize(vec3(-0.5, 0.75, -1.0));
	//vec3 col = vec3(0.44, 0.66, 0.98);
	vec3 col = vec3(0.1);
	vec3 sun = vec3(1.0, 0.9, 0.8);
	//vec3 sun = vec3(1.0);
	sun *= max(0.0, pow(dot(rd, light), 256.0));
	col *= max(0.0, dot(light, vec3(0.0, 0.0, -1.0)));
	return sun + col;
}*/

vec3 Trace(vec3 rc, vec3 rd, inout int id, inout float dist) {
	vec3 n;
	for(int i = 0; i < count; i++) {
		if (i == id) continue;
		vec3 tMin = (data[i].center.xyz - vec3(0.5) - rc) / rd;
		vec3 tMax = (data[i].center.xyz + vec3(0.5) - rc) / rd;
		vec3 t1 = min(tMin, tMax);
		vec3 t2 = max(tMin, tMax);
		float tNear = max(max(t1.x, t1.y), t1.z);
		float tFar = min(min(t2.x, t2.y), t2.z);
		if(tNear > tFar || tNear < 0.0) continue;
		if(dist > tNear || dist <= -1.0) {
			id = i;
			dist = tNear;
			n = -sign(rd) * step(t1.yzx, t1.xyz) * step(t1.zxy, t1.xyz);
		}
	}
	return n;
}

vec3 Ray(vec3 rc, vec3 rd) {
	vec3 col = vec3(0.0);
	vec3 recof = vec3(1.0);
	int id = -1;
	int Ref = 2;
	for(int i = 0; i <= Ref; i++) {
		float minIt = -1.0;
		vec3 norm = Trace(rc, rd, id, minIt);
		if(minIt <= -1.0) return col += vec3(0.5) * recof; //Sun(rd) * recof;
		col += data[id].color.rgb * recof;
		if(data[id].color.rgb == vec3(1.0)) return col;
		recof *= data[id].material.x;
		rc += rd * minIt;
		//rd = reflect(rd, norm);
		vec3 dif = abs(normalize(vec3(random(seed * inUV.xy), seed * random(inUV.yx), random(seed * inUV.yx))));
		rd = normalize(reflect(rd, norm) * (dif * data[id].material.x));
	}
	return col;
}


void main() {
	vec3 direction = normalize(vec3(1.0, (inUV - 0.5) * vec2(res[0], res[1]) / res[0]));
	direction.zx *= mat2(cos(-dir[1]), -sin(-dir[1]), sin(-dir[1]), cos(-dir[1]));
	direction.xy *= mat2(cos(dir[0]), -sin(dir[0]), sin(dir[0]), cos(dir[0]));
	vec3 col = Ray(pos, direction);
	fcolor = vec4(col, 1.0);
}