use bytemuck::{Pod, Zeroable};

#[repr(C)]
#[derive(Default, Copy, Clone, Debug, Zeroable, Pod)]
pub struct Voxel {
	pub center: [f32; 4],
	pub color: [f32; 4],
	pub material: [f32; 4],
}

impl Voxel {
	pub fn new(center: [f32; 4], color: [f32; 4], material: [f32; 4]) -> Self {
		Voxel {
			center,
			color,
			material,
		}
	}
}


#[repr(C)]
#[derive(Default, Copy, Clone, Zeroable, Pod)]
pub struct WorldData {
	pub rand: f32,
	pub count: u32,
}

impl WorldData {
	pub fn new(rand: f32, count: u32) -> Self {
		WorldData {
			rand,
			count,
		}
	}
}


#[repr(C)]
#[derive(Default, Copy, Clone, Zeroable, Pod)]
pub struct Camera {
	pub center: [f32; 3],
	pub direction: [f32; 2],
	pub res: [u32; 2],
	//pub fov: u32
}

impl Camera {
	pub fn new(x: f32, y: f32, z: f32, i: f32, j: f32, wight: u32, height: u32) -> Self {
		let center = [ x, y, z ];
		let direction = [ i, j ];
		let res = [ wight, height ];
		Camera {
			center,
			direction,
			//fov,
			res,
		}
	}
}