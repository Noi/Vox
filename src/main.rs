use rand::Rng;
use std::sync::Arc;
use vulkano::{
	buffer::{BufferUsage, CpuBufferPool},
	command_buffer::{
		AutoCommandBufferBuilder, CommandBufferUsage, RenderPassBeginInfo, SubpassContents,
	},
	descriptor_set::{PersistentDescriptorSet, WriteDescriptorSet},
	device::{
		physical::PhysicalDeviceType,
		Device, DeviceCreateInfo, DeviceExtensions, QueueCreateInfo,
	},
	image::{view::ImageView, ImageAccess, ImageUsage, SwapchainImage},
	instance::{Instance, InstanceCreateInfo},
	pipeline::{
		graphics::{
			input_assembly::InputAssemblyState,
			viewport::{Viewport, ViewportState},
		},
		GraphicsPipeline, Pipeline, PipelineBindPoint,
	},
	render_pass::{Framebuffer, FramebufferCreateInfo, RenderPass, Subpass},
	swapchain::{
		acquire_next_image, AcquireError, Swapchain, SwapchainCreateInfo, SwapchainCreationError, CompositeAlpha, PresentInfo,
	},
	sync::{self, FlushError, GpuFuture},
	VulkanLibrary,
};
use vulkano_win::VkSurfaceBuild;
use winit::{
	event::{DeviceEvent, ElementState, Event, KeyboardInput, WindowEvent},
	event_loop::{ControlFlow, EventLoop},
	window::{Window, WindowBuilder, CursorGrabMode},
};

use crate::data::Voxel;
use crate::data::WorldData;
use crate::data::Camera;
mod data;
struct Control {
	w: bool,
	s: bool,
	a: bool,
	d: bool,
	f: bool,
	space: bool,
	lsift: bool,
	ctrl: bool,
}

fn main() {
	let library = VulkanLibrary::new().unwrap();
	let required_ext = vulkano_win::required_extensions(&library);

	let instance = Instance::new(library, InstanceCreateInfo {
		enabled_extensions: required_ext,

		enumerate_portability: true,
		..Default::default()
	})
	.unwrap();


	let event_loop = EventLoop::new();
	let surface = WindowBuilder::new()
		.with_title("Vox")
		.build_vk_surface(&event_loop, instance.clone())
		.unwrap();


	let device_ext = DeviceExtensions {
		khr_swapchain: true,
		..DeviceExtensions::empty()
	};


	let (physical_device, queue_family_index) = instance
		.enumerate_physical_devices()
		.unwrap()
		.filter(|p| {
			p.supported_extensions().contains(&device_ext)
		})
		.filter_map(|p| {
			p.queue_family_properties()
				.iter()
				.enumerate()
				.position(|(i, q)| {
					q.queue_flags.graphics && p.surface_support(i as u32, &surface).unwrap_or(false)
				})
				.map(|i| (p, i as u32))
		})
		.min_by_key(|(p, _)| match p.properties().device_type {
			PhysicalDeviceType::DiscreteGpu => 0,
			PhysicalDeviceType::IntegratedGpu => 1,
			PhysicalDeviceType::VirtualGpu => 2,
			PhysicalDeviceType::Cpu => 3,
			PhysicalDeviceType::Other => 4,
			_ => 5,
		})
		.expect("No suitable physical device found");

	println!(
		"Using device: {} (type: {:?})",
		physical_device.properties().device_name,
		physical_device.properties().device_type,
	);

	let (device, mut queues) = Device::new(
		physical_device,
		DeviceCreateInfo {
			enabled_extensions: device_ext,

			queue_create_infos: vec![QueueCreateInfo {
				queue_family_index,
				..Default::default()
			}],

			..Default::default()
		},
	)
	.unwrap();

	let queue = queues.next().unwrap();

	let (mut swapchain, images) = {
		let surface_capabilities = device
			.physical_device()
			.surface_capabilities(&surface, Default::default())
			.unwrap();

		let image_format = Some(
			device
				.physical_device()
				.surface_formats(&surface, Default::default())
				.unwrap()[0]
				.0,
		);

		Swapchain::new(
			device.clone(),
			surface.clone(),
			SwapchainCreateInfo {
				min_image_count: surface_capabilities.min_image_count,

				image_format,

				image_extent: surface.window().inner_size().into(),

				image_usage: ImageUsage {
					color_attachment: true,
					..ImageUsage::empty()
				},

				composite_alpha: CompositeAlpha::Opaque,

				..Default::default()
			},
		)
		.unwrap()
	};

	let mut rng = rand::thread_rng();

	let mut camera = Camera::new(
		0.0,
		0.0,
		0.0,
		0.0,
		0.0,
		surface.window().inner_size().width,
		surface.window().inner_size().height,
	);

	let mut control = Control {
		w: false,
		s: false,
		a: false,
		d: false,
		f: false,
		space: false,
		lsift: false,
		ctrl: false,
	};

	let mut cursor: (f64, f64) = (0.0, 0.0);
	let mut cursor_hide = false;

	let mut voxels = Vec::new();
	voxels.push(Voxel::new([-1.0, -1.0, 0.0, 0.0], [0.5, 0.5, 0.5, 0.0], [0.5, 0.5, 1.0, 0.0]));
	voxels.push(Voxel::new([0.0, -1.0, 0.0, 0.0], [0.0, 1.0, 0.1, 0.0], [0.5, 0.5, 1.0, 0.0]));
	voxels.push(Voxel::new([1.0, -1.0, 0.0, 0.0], [1.0, 0.0, 0.0, 0.0], [0.5, 0.5, 1.0, 0.0]));
	voxels.push(Voxel::new([-1.0, 0.0, 0.0, 0.0], [0.0, 1.0, 0.5, 0.0], [0.5, 0.5, 1.0, 0.0]));
	voxels.push(Voxel::new([0.0, 0.0, 0.0, 0.0], [0.5, 0.5, 0.5, 0.0], [0.5, 0.5, 1.0, 0.0]));
	voxels.push(Voxel::new([1.0, 0.0, 0.0, 0.0], [0.0, 1.0, 0.1, 0.0], [0.5, 0.5, 1.0, 0.0]));
	voxels.push(Voxel::new([-1.0, 1.0, 0.0, 0.0], [1.0, 0.0, 0.0, 0.0], [0.5, 0.5, 1.0, 0.0]));
	voxels.push(Voxel::new([0.0, 1.0, 0.0, 0.0], [0.0, 1.0, 0.5, 0.0], [0.5, 0.5, 1.0, 0.0]));
	voxels.push(Voxel::new([1.0, 1.0, 0.0, 0.0], [0.0, 1.0, 0.5, 0.0], [0.5, 0.5, 1.0, 0.0]));

	voxels.push(Voxel::new([-1.0, -2.0, -1.0, 0.0], [1.0, 0.0, 1.0, 0.0], [0.5, 0.5, 1.0, 0.0]));
	voxels.push(Voxel::new([0.0, -2.0, -1.0, 0.0], [1.0, 0.3, 0.0, 0.0], [0.5, 0.5, 1.0, 0.0]));
	voxels.push(Voxel::new([1.0, -2.0, -1.0, 0.0], [0.3, 1.0, 0.1, 0.0], [0.5, 0.5, 1.0, 0.0]));
	voxels.push(Voxel::new([-1.0, -2.0, -2.0, 0.0], [1.0, 0.5, 0.5, 0.0], [0.8, 0.5, 1.0, 0.0]));
	voxels.push(Voxel::new([0.0, -2.0, -2.0, 0.0], [0.3, 1.0, 0.1, 0.0], [0.5, 0.5, 1.0, 0.0]));
	voxels.push(Voxel::new([1.0, -2.0, -2.0, 0.0], [1.0, 0.5, 0.5, 0.0], [0.7, 0.5, 1.0, 0.0]));
	voxels.push(Voxel::new([-1.0, -2.0, -3.0, 0.0], [0.3, 1.0, 0.1, 0.0], [0.5, 0.5, 1.0, 0.0]));
	voxels.push(Voxel::new([0.0, -2.0, -3.0, 0.0], [1.0, 0.5, 0.5, 0.0], [0.6, 0.5, 1.0, 0.0]));
	voxels.push(Voxel::new([1.0, -2.0, -3.0, 0.0], [0.5, 0.5, 0.5, 0.0], [0.5, 0.5, 1.0, 0.0]));

	voxels.push(Voxel::new([2.0, -1.0, -1.0, 0.0], [1.0, 0.0, 1.0, 0.0], [0.5, 0.5, 1.0, 0.0]));
	voxels.push(Voxel::new([2.0, 0.0, -1.0, 0.0], [1.0, 0.3, 0.0, 0.0], [0.5, 0.5, 1.0, 0.0]));
	voxels.push(Voxel::new([2.0, 1.0, -1.0, 0.0], [0.3, 1.0, 0.1, 0.0], [0.5, 0.5, 1.0, 0.0]));
	voxels.push(Voxel::new([2.0, -1.0, -2.0, 0.0], [1.0, 0.5, 0.5, 0.0], [0.8, 0.5, 1.0, 0.0]));
	voxels.push(Voxel::new([2.0, 0.0, -2.0, 0.0], [0.3, 1.0, 0.1, 0.0], [0.5, 0.5, 1.0, 0.0]));
	voxels.push(Voxel::new([2.0, 1.0, -2.0, 0.0], [1.0, 0.5, 0.5, 0.0], [0.7, 0.5, 1.0, 0.0]));
	voxels.push(Voxel::new([2.0, -1.0, -3.0, 0.0], [0.3, 1.0, 0.1, 0.0], [0.5, 0.5, 1.0, 0.0]));
	voxels.push(Voxel::new([2.0, 0.0, -3.0, 0.0], [1.0, 0.5, 0.5, 0.0], [0.6, 0.5, 1.0, 0.0]));
	voxels.push(Voxel::new([2.0, 1.0, -3.0, 0.0], [0.5, 0.5, 0.5, 0.0], [0.5, 0.5, 1.0, 0.0]));

	voxels.push(Voxel::new([-1.0, 2.0, -1.0, 0.0], [1.0, 0.0, 1.0, 0.0], [0.5, 0.5, 1.0, 0.0]));
	voxels.push(Voxel::new([0.0, 2.0, -1.0, 0.0], [1.0, 0.3, 0.0, 0.0], [0.5, 0.5, 1.0, 0.0]));
	voxels.push(Voxel::new([1.0, 2.0, -1.0, 0.0], [0.3, 1.0, 0.1, 0.0], [0.5, 0.5, 1.0, 0.0]));
	voxels.push(Voxel::new([-1.0, 2.0, -2.0, 0.0], [1.0, 0.5, 0.5, 0.0], [0.8, 0.5, 1.0, 0.0]));
	voxels.push(Voxel::new([0.0, 2.0, -2.0, 0.0], [0.3, 1.0, 0.1, 0.0], [0.5, 0.5, 1.0, 0.0]));
	voxels.push(Voxel::new([1.0, 2.0, -2.0, 0.0], [1.0, 0.5, 0.5, 0.0], [0.7, 0.5, 1.0, 0.0]));
	voxels.push(Voxel::new([-1.0, 2.0, -3.0, 0.0], [0.3, 1.0, 0.1, 0.0], [0.5, 0.5, 1.0, 0.0]));
	voxels.push(Voxel::new([0.0, 2.0, -3.0, 0.0], [1.0, 0.5, 0.5, 0.0], [0.6, 0.5, 1.0, 0.0]));
	voxels.push(Voxel::new([1.0, 2.0, -3.0, 0.0], [0.5, 0.5, 0.5, 0.0], [0.5, 0.5, 1.0, 0.0]));

	voxels.push(Voxel::new([-2.0, -1.0, -1.0, 0.0], [1.0, 0.0, 1.0, 0.0], [0.5, 0.5, 1.0, 0.0]));
	voxels.push(Voxel::new([-2.0, 0.0, -1.0, 0.0], [1.0, 0.3, 0.0, 0.0], [0.5, 0.5, 1.0, 0.0]));
	voxels.push(Voxel::new([-2.0, 1.0, -1.0, 0.0], [0.3, 1.0, 0.1, 0.0], [0.5, 0.5, 1.0, 0.0]));
	voxels.push(Voxel::new([-2.0, -1.0, -2.0, 0.0], [1.0, 0.5, 0.5, 0.0], [0.8, 0.5, 1.0, 0.0]));
	voxels.push(Voxel::new([-2.0, 0.0, -2.0, 0.0], [0.3, 1.0, 0.1, 0.0], [0.5, 0.5, 1.0, 0.0]));
	voxels.push(Voxel::new([-2.0, 1.0, -2.0, 0.0], [1.0, 0.5, 0.5, 0.0], [0.7, 0.5, 1.0, 0.0]));
	voxels.push(Voxel::new([-2.0, -1.0, -3.0, 0.0], [0.3, 1.0, 0.1, 0.0], [0.5, 0.5, 1.0, 0.0]));
	voxels.push(Voxel::new([-2.0, 0.0, -3.0, 0.0], [1.0, 0.5, 0.5, 0.0], [0.6, 0.5, 1.0, 0.0]));
	voxels.push(Voxel::new([-2.0, 1.0, -3.0, 0.0], [0.5, 0.5, 0.5, 0.0], [0.5, 0.5, 1.0, 0.0]));

	voxels.push(Voxel::new([-1.0, -1.0, -4.0, 0.0], [0.5, 0.5, 0.5, 0.0], [0.5, 0.5, 1.0, 0.0]));
	voxels.push(Voxel::new([0.0, -1.0, -4.0, 0.0], [0.0, 1.0, 0.1, 0.0], [0.5, 0.5, 1.0, 0.0]));
	voxels.push(Voxel::new([1.0, -1.0, -4.0, 0.0], [1.0, 0.0, 0.0, 0.0], [0.5, 0.5, 1.0, 0.0]));
	voxels.push(Voxel::new([-1.0, 0.0, -4.0, 0.0], [0.0, 1.0, 0.5, 0.0], [0.5, 0.5, 1.0, 0.0]));
	voxels.push(Voxel::new([0.0, 0.0, -4.0, 0.0], [0.0, 0.5, 0.5, 0.0], [0.5, 0.5, 1.0, 0.0]));
	voxels.push(Voxel::new([1.0, 0.0, -4.0, 0.0], [0.0, 1.0, 0.1, 0.0], [0.5, 0.5, 1.0, 0.0]));
	voxels.push(Voxel::new([-1.0, 1.0, -4.0, 0.0], [1.0, 0.0, 0.0, 0.0], [0.5, 0.5, 1.0, 0.0]));
	voxels.push(Voxel::new([0.0, 1.0, -4.0, 0.0], [0.0, 1.0, 0.5, 0.0], [0.5, 0.5, 1.0, 0.0]));
	voxels.push(Voxel::new([1.0, 1.0, -4.0, 0.0], [1.0, 1.0, 1.0, 0.0], [0.5, 0.5, 1.0, 0.0]));

	voxels.push(Voxel::new([0.0, 0.0, -1.0, 0.0], [0.2, 0.6, 0.1, 0.0], [0.5, 0.5, 1.0, 0.0]));


	let buffer = CpuBufferPool::new(
		device.clone(), 
		BufferUsage {
			storage_buffer: true,
			..BufferUsage::empty()
		},
	);
	let pos_buffer = CpuBufferPool::new(
		device.clone(), 
		BufferUsage {
			storage_buffer: true,
			..BufferUsage::empty()
		},
	);
	let world_buffer = CpuBufferPool::new(
		device.clone(), 
		BufferUsage {
			storage_buffer: true,
			..BufferUsage::empty()
		},
	);

	let fs = fs::load(device.clone()).unwrap();
	let vs = vs::load(device.clone()).unwrap();


	let render_pass = vulkano::single_pass_renderpass!(
		device.clone(),
		attachments: {
			color: {
				load: Clear,
				store: Store,
				format: swapchain.image_format(),
				samples: 1,
			}
		},
		pass: {
			color: [color],
			depth_stencil: {}
		}
	)
	.unwrap();

	let pipeline = GraphicsPipeline::start()
		.vertex_shader(vs.entry_point("main").unwrap(), ())
		.input_assembly_state(InputAssemblyState::new())
		.viewport_state(ViewportState::viewport_dynamic_scissor_irrelevant())
		.fragment_shader(fs.entry_point("main").unwrap(), ())
		.render_pass(Subpass::from(render_pass.clone(), 0).unwrap())
		.build(device.clone())
		.unwrap();


	let mut viewport = Viewport {
		origin: [0.0, 0.0],
		dimensions: [0.0, 0.0],
		depth_range: 0.0..1.0,
	};

	let mut framebuffers = window_size_dependent_setup(&images, render_pass.clone(), &mut viewport);

	let mut recreate_swapchain = false;

	let mut previous_frame_end = Some(sync::now(device.clone()).boxed());

	event_loop.run(move |event, _, control_flow| {
		match event {
			Event::WindowEvent { event, .. } => {
				match event {
					WindowEvent::CloseRequested => *control_flow = ControlFlow::Exit,
					WindowEvent::Resized(_) => recreate_swapchain = true,
					WindowEvent::KeyboardInput {
						input:
							KeyboardInput {
								state: ElementState::Pressed,
								virtual_keycode: Some(key),
								..
							},
						..
					} => {
						use winit::event::VirtualKeyCode::*;
						match key {
							Escape => {
								surface.window().set_cursor_grab(CursorGrabMode::None).unwrap();
								surface.window().set_cursor_visible(true);
								cursor_hide = false;
							},
							W => control.w = true,
							S => control.s = true,
							A => control.a = true,
							D => control.d = true,
							F => control.f = true,
							Space => control.space = true,
							LShift => control.lsift = true,
							LControl => control.ctrl = true,
							_ => (),
						}
					}
					WindowEvent::KeyboardInput {
						input:
							KeyboardInput {
								state: ElementState::Released,
								virtual_keycode: Some(key),
								..
							},
						..
					} => {
						use winit::event::VirtualKeyCode::*;
						match key {
							W => control.w = false,
							S => control.s = false,
							A => control.a = false,
							D => control.d = false,
							F => control.f = false,
							Space => control.space = false,
							LShift => control.lsift = false,
							LControl => control.ctrl = false,
							_ => (),
						}
					}
					WindowEvent::MouseInput {
						state: ElementState::Pressed,
						button,
						..
					} => {
						use winit::event::MouseButton::*;
						match button {
							Left => {
								surface.window().set_cursor_grab(CursorGrabMode::Confined).unwrap();
								surface.window().set_cursor_visible(false);
								cursor_hide = true;
							}
							Right => (), //TO DO
							_ => (),
						}
					}
					_ => (),
				}
			}
			Event::DeviceEvent {
				event: DeviceEvent::MouseMotion { delta },
				..
			} => {
				if cursor_hide {
					cursor.0 = cursor.0 + delta.0;
					cursor.1 = (cursor.1 + delta.1).max(-180.0).min(180.0);
				}
			}
			Event::RedrawEventsCleared => {
				let win_size = surface.window().inner_size();
				if win_size.width == 0 || win_size.height == 0 {
					return;
				}
				camera.res = [win_size.width, win_size.height];

				previous_frame_end.as_mut().unwrap().cleanup_finished();

				if recreate_swapchain {
					let (new_swapchain, new_images) =
						match swapchain.recreate(SwapchainCreateInfo {
							image_extent: win_size.into(),
							..swapchain.create_info()
						}) {
							Ok(r) => r,
							Err(SwapchainCreationError::ImageExtentNotSupported { .. }) => return,
							Err(e) => panic!("Failed to recreate swapchain: {:?}", e),
						};

					swapchain = new_swapchain;
					framebuffers = window_size_dependent_setup(
						&new_images,
						render_pass.clone(),
						&mut viewport,
					);
					recreate_swapchain = false;
				}

				let (image_num, suboptimal, acquire_future) =
					match acquire_next_image(swapchain.clone(), None) {
						Ok(r) => r,
						Err(AcquireError::OutOfDate) => {
							recreate_swapchain = true;
							return;
						}
						Err(e) => panic!("Failed to acquire next image: {:?}", e),
					};

				if suboptimal {
					recreate_swapchain = true;
				}

				if cursor_hide {
					camera.direction[0] = (cursor.0 * 0.01) as f32;
					camera.direction[1] = (cursor.1 * 0.01) as f32;
					let mut dir = vec![0.0, 0.0, 0.0];
					let mut dir_temp = vec![0.0, 0.0, 0.0];
					let speed = 0.01;
					if control.w {dir[0] += 1.0};
					if control.s {dir[0] += -1.0};
					if control.a {dir[1] += -1.0};
					if control.d {dir[1] += 1.0};
					dir_temp[0] = (dir[2] * (-camera.direction[1]).sin() + dir[0] * (-camera.direction[1]).cos()) * camera.direction[0].cos() - dir[1] * camera.direction[0].sin();
					dir_temp[1] = (dir[2] * (-camera.direction[1]).sin() + dir[0] * (-camera.direction[1]).cos()) * camera.direction[0].sin() + dir[1] * camera.direction[0].cos();
					dir_temp[2] = dir[2] * (-camera.direction[1]).cos() - dir[0] * (-camera.direction[1]).sin();
					camera.center = [camera.center[0] + (dir_temp[0] * speed), camera.center[1] + (dir_temp[1] * speed), camera.center[2] + (dir_temp[2] * speed)];
					if control.space {camera.center[2] -= speed};
					if control.lsift {camera.center[2] += speed};
				}

				let world_data = WorldData::new(rng.gen(), voxels.len() as u32);
				let world_sub_buffer = world_buffer.from_data(world_data).unwrap();
				let sub_buffer = buffer.from_iter(voxels.clone().into_iter()).unwrap();
				let pos_sub_buffer = pos_buffer.from_data(camera).unwrap();
				let layout = pipeline.layout().set_layouts().get(0).unwrap();
				let descriptor = PersistentDescriptorSet::new(
					layout.clone(),
					[
						WriteDescriptorSet::buffer(0, sub_buffer.clone()),
						WriteDescriptorSet::buffer(1, pos_sub_buffer.clone()),
						WriteDescriptorSet::buffer(2, world_sub_buffer.clone()),
					],
				)
				.unwrap();

				let mut builder = AutoCommandBufferBuilder::primary(
					device.clone(),
					queue.queue_family_index(),
					CommandBufferUsage::OneTimeSubmit,
				)
				.unwrap();

				let clear_values = vec![Some([0.0, 0.0, 0.0, 1.0].into())];

				builder
					.bind_descriptor_sets(
						PipelineBindPoint::Graphics,
						pipeline.layout().clone(),
						0,
						descriptor.clone(),
					)
					.begin_render_pass(
						RenderPassBeginInfo {
							clear_values,
							..RenderPassBeginInfo::framebuffer(framebuffers[image_num].clone())
						},
						SubpassContents::Inline,
					)
					.unwrap()
					.set_viewport(0, [viewport.clone()])
					.bind_pipeline_graphics(pipeline.clone())
					.draw(3, 1, 0, 0)
					.unwrap()
					.end_render_pass()
					.unwrap();

				let command_buffer = builder.build().unwrap();

				let future = previous_frame_end
					.take()
					.unwrap()
					.join(acquire_future)
					.then_execute(queue.clone(), command_buffer)
					.unwrap()
					.then_swapchain_present(
						queue.clone(),
						PresentInfo {
							index: image_num,
							..PresentInfo::swapchain(swapchain.clone())
						},
					)
					.then_signal_fence_and_flush();

				match future {
					Ok(future) => {
						previous_frame_end = Some(future.boxed());
					}
					Err(FlushError::OutOfDate) => {
						recreate_swapchain = true;
						previous_frame_end = Some(sync::now(device.clone()).boxed());
					}
					Err(e) => {
						println!("Failed to flush future: {:?}", e);
						previous_frame_end = Some(sync::now(device.clone()).boxed());
					}
				}
			}
			_ => (),
		}
	});
}

fn window_size_dependent_setup(
	images: &[Arc<SwapchainImage<Window>>],
	render_pass: Arc<RenderPass>,
	viewport: &mut Viewport,
) -> Vec<Arc<Framebuffer>> {
	let dimensions = images[0].dimensions().width_height();
	viewport.dimensions = [dimensions[0] as f32, dimensions[1] as f32];

	images
		.iter()
		.map(|image| {
			let view = ImageView::new_default(image.clone()).unwrap();
			Framebuffer::new(
				render_pass.clone(),
				FramebufferCreateInfo {
					attachments: vec![view],
					..Default::default()
				},
			)
			.unwrap()
		})
		.collect::<Vec<_>>()
}

mod fs {
	vulkano_shaders::shader! {
		ty: "fragment",
		path: "src/fragment.glsl",
		types_meta: {
			use bytemuck::{Pod, Zeroable};

			#[derive(Clone, Copy, Zeroable, Pod)]
		},
	}
}
mod vs {
	vulkano_shaders::shader! {
		ty: "vertex",
		src: "
		#version 450

		layout (location = 0) out vec2 outUV;

		void main() {
			outUV = vec2((gl_VertexIndex << 1) & 2, gl_VertexIndex & 2);
			gl_Position = vec4(outUV * 2.0f + -1.0f, 0.0f, 1.0f);
		}",
	}
}